This is a fork of Adafruit's fork of JeeLab's fantastic real time clock library for Arduino.

Forked to optimize for DS3231 and add additional features such as 12/24hr, alarms, temperature, etc.

To use:
Place the RTClib folder in your *arduinosketchfolder*/libraries/ folder. 
